# Artis Virgo

Submit scripts using local storage for the 3D Monte Carlo radiative transfer code [Artis] (https://github.com/artis-mcrt/artis).  

## Rationale
The MPI code [Artis] (https://github.com/artis-mcrt/artis) uses a file-per-process I/O pattern which tends to be
inefficient and unstable on  `Lustre` shared file system. 

Additionally the codes generate a lot of small output files and performs `write` (using C I/O commands `fprintf(...)`, `fwrite(...)`) but
also `read`  (using C I/O commands `fread(...) during runtime.

The code itself shows excellent scalability properties but suffers from sporadical drop of performance caused by the dedicated I/O pattern used
internally.



## Submit scipts
New submit scripts have been written in order to make use of the local storage `/tmp` on the nodes participating in the MPI job.
This is possible since
the Artis code do not use parallel I/O (MPI I/O) and do not share files between processes.
The scripts are available on `/scripts` directory in this repository:

```
- set_packages.sh
- artis-local.sh
- artis-local-submit.sh

```

## artis-local.sh
This script defines a local storage `$MYTMP` as `/tmp/$USER/$SLURM_JOB_ID` in which all the relevant input files and executables for Artis to run will be copied.
When submitted to the queue system, variable $USER will be translated into your user name, and `$SLURM_JOB_ID` will get the job ID number.
For example, when a user `collins` submits a new job that gets `job ID 4423` assigned by `SLURM`, the `$MYTMP` on the computational node becomes `/tmp/collins/4423`.
This would eliminate a chance of overwriting the `$MYTMP` content by another job that user `collins` may submit later.
When Artis completes, all the results files i.e

```
- estimators*.out`
- output_*.out
- packets00*.out
- packets0*.tmp
```
will be copied back the a new created directory named `output_$SLURM_JOB_ID` which will then contain all the Artis output files with exactly the same file layout.

A post-analysis can then be done without any code modification.

## artis-local.sh copy mechanism

Two copy options are supported

```bash
- ./artis-local.sh -c cp # direct copy
- ./artis-local.sh -c tar # archive + copy
```

- `Direct copy mechanism`: all Artis ouput files will be sequentially copied to the newly
created `output_$SLURM_JOB_ID` Lustre directory.

- `Archiving + copy`: the local `/tmp/$USER/$SLURM_JOB_ID` is first archived using the `tar` mechanism and then only the archive is copied to the `Lustre` output directory. This option could be usefull when the job is running on  mutiple nodes to avoid too much I/O traffic from local to share filesystem.    


## artis-local-submit.sh
The script can be submitted to the cluster queue with `SLURM` command `sbatch` using the main submit scripts `artis-local-submit.sh`.
This scripts sets once the software dependencies i.e mainly `gcc`,`openMPI`, `gsl` external libraries.

## Integrate with Artis
To integrate with your Artis program, simply copy all the scripts on the main Artis working directory on `Lustre` and submit from this
directory.
SLURM output and error will be  redirected as

```
- $SLURM_JOB_ID.out.log
- $SLURM_JOB_ID.err.log
```
When everything works, a typical ouput for `10 nodes` would be:

```
spack loading openmpi
spack loading gcc
spack loading gsl
gcc is /cvmfs/vae.gsi.de/centos7/spack-0.17/opt/linux-centos7-x86_64/gcc-4.8.5/gcc-8.1.0-nswpump2zjkpne3ipmxkqt75dq6s2g7w/bin/gcc
mpicc is /cvmfs/vae.gsi.de/centos7/spack-0.17/opt/linux-centos7-x86_64/gcc-8.1.0/openmpi-3.1.6-55d4p7423fcg6figm3s67efkv5vlefcc/bin/mpicc
creating dir:  /tmp/dbertini/51281895  on node: lxbk1034
creating dir:  /tmp/dbertini/51281895  on node: lxbk1036
creating dir:  /tmp/dbertini/51281895  on node: lxbk1037
creating dir:  /tmp/dbertini/51281895  on node: lxbk1050
creating dir:  /tmp/dbertini/51281895  on node: lxbk1075
creating dir:  /tmp/dbertini/51281895  on node: lxbk1048
creating dir:  /tmp/dbertini/51281895  on node: lxbk1035
creating dir:  /tmp/dbertini/51281895  on node: lxbk1073
creating dir:  /tmp/dbertini/51281895  on node: lxbk1074
creating dir:  /tmp/dbertini/51281895  on node: lxbk1049
copying bak to dir:  /lustre/rz/dbertini/ccollins/test_run/output_51281895  from node:  lxbk1034
copying bak to dir:  /lustre/rz/dbertini/ccollins/test_run/output_51281895  from node:  lxbk1075
copying bak to dir:  /lustre/rz/dbertini/ccollins/test_run/output_51281895  from node:  lxbk1074
copying bak to dir:  /lustre/rz/dbertini/ccollins/test_run/output_51281895  from node:  lxbk1048
copying bak to dir:  /lustre/rz/dbertini/ccollins/test_run/output_51281895  from node:  lxbk1050
copying bak to dir:  /lustre/rz/dbertini/ccollins/test_run/output_51281895  from node:  lxbk1036
copying bak to dir:  /lustre/rz/dbertini/ccollins/test_run/output_51281895  from node:  lxbk1037
copying bak to dir:  /lustre/rz/dbertini/ccollins/test_run/output_51281895  from node:  lxbk1035
copying bak to dir:  /lustre/rz/dbertini/ccollins/test_run/output_51281895  from node:  lxbk1073
copying bak to dir:  /lustre/rz/dbertini/ccollins/test_run/output_51281895  from node:  lxbk1049
removing local directory: /tmp/dbertini/51281895  on node: lxbk1075
removing local directory: /tmp/dbertini/51281895  on node: lxbk1035
removing local directory: /tmp/dbertini/51281895  on node: lxbk1073
removing local directory: /tmp/dbertini/51281895  on node: lxbk1036
removing local directory: /tmp/dbertini/51281895  on node: lxbk1074
removing local directory: /tmp/dbertini/51281895  on node: lxbk1050
removing local directory: /tmp/dbertini/51281895  on node: lxbk1048
removing local directory: /tmp/dbertini/51281895  on node: lxbk1049
removing local directory: /tmp/dbertini/51281895  on node: lxbk1037
removing local directory: /tmp/dbertini/51281895  on node: lxbk1034
```


## Cleanup scripts
If for any reason your job crashed or has been cancelled, etc.., you will need to clean up the
`/tmp` directories on the cluster nodes which were used.
To ease this process one can find in the `utils` directory a cleanup script.
To use it you will need to give as arguments

- how many `days` to look back in time

- the `job_name`

For example to cleanup the `/tmp` used by all jobs with name `artis_l` during the last 2 days:

```
./cleanup.sh -d 2 -j artis_l

```
giving the output:

```
Cleanup jobs with job_name:  artis-l from date:  2022-05-28
cleanup will execute on nodelist:  lxbk[1047-1056]  corresponding to:  10  nodes.
Submitted batch job 52711719
```


