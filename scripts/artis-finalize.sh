#!/bin/bash 


OPTIND=1

usage() { echo "Usage: $0  [-c <string>]" 1>&2; return; }


while getopts ":v:c:" o; do
    case "${o}" in
        v)
            v=${OPTARG}
            ;;
        c)
            c=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

if [ -z "${c}" ]; then
    usage
fi

# Add utility functions
source ./utils.shinc

myhost=`hostname`

echo "Artis-finalize: copy mechanism: ${c} JobId: ${SLURM_JOB_ID} on host: ${myhost}"

# Source directory
MYHDIR=$SLURM_SUBMIT_DIR

# Destination directory
# local scratch directory on the node
MYTMP="/tmp/$USER/$SLURM_JOB_ID"


if [ "$SLURM_LOCALID" -eq "0" ]
then
    OUTPUT=$MYHDIR/output_${SLURM_JOB_ID};
    ARCHIVE=output_${SLURM_JOB_ID}_$myhost.tar;
    
    # start time to copy
    start=$(date +%s)   
    
    if [ $c == 'cp' ]
    then
	echo "copying back to dir: " $OUTPUT " from node: " $myhost;
	mkdir -p $OUTPUT
	cp $MYTMP/estimators* $OUTPUT;
	cp $MYTMP/output_* $OUTPUT;
	cp $MYTMP/packets* $OUTPUT;
	time_elapsed "$(($(date +%s) - ${start}))"
    elif [ $c == 'tar' ]
    then
	echo "copying archive: " $ARCHIVE " to dir: " $OUTPUT " from node: " $myhost;
	mkdir -p $OUTPUT
	tar cf $ARCHIVE $MYTMP
	cp $ARCHIVE $OUTPUT
	time_elapsed "$(($(date +%s) - ${start}))"	
    fi
    
    echo "removing local directory:" $MYTMP " on node:" $myhost;
    rm -rf $MYTMP;                      
    rm -rf /tmp/${USER};
    echo "removed local directory:" $MYTMP " on node:" $myhost;

    if [ -f  $MYHDIR/input-artis.tar.gz ]; then
        rm -rf 	$MYHDIR/input-artis.tar.gz
    fi
fi



