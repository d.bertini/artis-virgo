#!/bin/bash 


# Add utility functions
source ./utils.shinc

myhost=`hostname`

# Submit directory 
MYHDIR=$SLURM_SUBMIT_DIR

# Local scratch directory on the node
MYTMP="/tmp/$USER/$SLURM_JOB_ID"

#echo " node: " $SLURM_NODEID " localid: " $SLURM_LOCALID 

# Archiving input file for Artis simulation  context
if  [ "$SLURM_NODEID" -eq "0" ] && [ "$SLURM_LOCALID" -eq "0" ]; then
    cd $MYHDIR
    tar --exclude='*.log'  --exclude='*.out' --exclude='output*'  --exclude='artis' --exclude='*~' --exclude='*.gz' -zcf input-artis.tar.gz .
    echo " archive created ... "
fi



