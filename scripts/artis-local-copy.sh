#!/bin/bash 

# Add utility functions
source ./utils.shinc

myhost=`hostname`

# Submit directory 
MYHDIR=$SLURM_SUBMIT_DIR

# Local scratch directory on the node
MYTMP="/tmp/$USER/$SLURM_JOB_ID"

if [ "$SLURM_LOCALID" -eq "0" ]; then
   cd $MYHDIR
   echo "creating dir: " $MYTMP " on node:" $myhost;

   mkdir -p $MYTMP;
   cp input-artis.tar.gz $MYTMP
   sleep 5
   
   while true
   do
       if [ -f  $MYTMP/input-artis.tar.gz ]; then
	   
	   # Stat archive files
	   # This should be avoided but we have to control
	   # copied data intergrity
	   
	   input_source=$(stat -c%s $MYHDIR/input-artis.tar.gz)
	   dest_source=$(stat -c%s $MYTMP/input-artis.tar.gz)

	   if [ ${input_source} -eq ${dest_source} ]; then
	       echo " "
	       echo "Archive copy  OK, proceed with computation on host: " $myhost
	       echo " "
	       cd $MYTMP
	       tar xvfz input-artis.tar.gz       
               break
	   else
	       echo " "    
	       echo "Archive copy error, waiting on host: " $myhost
	       echo " "
	       sleep 5
	   fi
	   
       else 
	   echo "Waiting for archiving process to finish ..."
	   sleep 5
       fi
   done
fi

