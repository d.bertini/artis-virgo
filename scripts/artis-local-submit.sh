#!/bin/bash

#SBATCH --job-name=artis-l
#SBATCH --time=00-08:00:00
#SBATCH --output=%j.out.log
#SBATCH --error=%j.err.log
#SBATCH --partition=main
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=10
#SBATCH --mem-per-cpu=4000
# #SBATCH --mem=16gb #  N_t (threads) * requiring Mem_t (1000 MBytes)
#SBATCH --mail-type=ALL
#SBATCH --mail-user=${USER}@gsi.de
#SBATCH --no-requeue
#SBATCH --cpus-per-task=1
#SBATCH --export=ALL,EXECUTABLE=./sn3d.exe

export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
export NUM_CORES=${SLURM_NTASKS}*${SLURM_CPUS_PER_TASK}

export SINGULARITY_NO_NAMESPACE_PID=1

echo "Job configuration: "
echo "${EXECUTABLE} running on ${NUM_CORES} cores with ${SLURM_NTASKS} MPI-tasks and ${OMP_NUM_THREADS} threads"
echo " "

# define scripts to run
init_script=artis-initialize.sh
copy_script=artis-local-copy.sh
finalize_script=artis-finalize.sh
run_script=artis-run.sh

# loading softw-stack for sn3dg
. ./set_packages.sh


# run tasks-scripts
srun --mpi=pmix_v2  --export=ALL -- ./$init_script 
srun --mpi=pmix_v2  --export=ALL -- ./$copy_script 

#ret=`grep -i "error" ${SLURM_JOB_ID}.out.log`

#if [ -z ${ret} ]; then
#    echo " "
#    echo "Initialisation completed, proceed with computation ..."
#    echo " "
#else
#    exit
#fi

srun --mpi=pmix_v2  --export=ALL -- ./$run_script 

srun --mpi=pmix_v2  --export=ALL -- ./$finalize_script -c cp

