#!/bin/bash 


# Source directory
MYHDIR=$SLURM_SUBMIT_DIR

# Destination directory
# local scratch directory on the node
MYTMP="/tmp/$USER/$SLURM_JOB_ID"


# execute Artis simulation locally
cd $MYTMP
./sn3d.exe > out.txt


