# Load needed packages
unset LD_LIBRARY_PATH

echo "spack loading openmpi"
spack load  openmpi%gcc arch=x86_64    
echo "spack loading gcc"
spack load gcc    
echo "spack loading gsl"
spack load gsl%gcc arch=x86_64

type gcc
type mpicc
