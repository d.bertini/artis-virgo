#!/bin/bash 


OPTIND=1

usage() { echo "Usage: $0  [-d <days> -j <job_name>]" 1>&2; return; }


while getopts ":d:j:" o; do
    case "${o}" in
        d)
            d=${OPTARG}
            ;;
        j)
            j=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

if [ -z "${d}" ] && [ -z "${j}" ]; then
    usage
    exit 
fi


pdate=`date -d "${d} day ago" '+%Y-%m-%d'`

echo "Cleanup jobs with job_name: " $j "from date: " $pdate

arr_jid=$( sacct -n -X --format jobid --name $j -S $pdate )

s_none='None'

for i in $arr_jid
do
    #echo "${i}"
    used_nodes=$( sacct -n -X -j "${i}" --format Node%500f | xargs )
    #echo $used_nodes
    
    if [[ $used_nodes == *"$s_none"* ]]; then
	echo "Found job with None assigned nodes ... skiping .. "
    else
	nlist=$used_nodes	
    fi

done
#echo $used_nodes

# get the number of nodes
t_nodes=`sinfo  -h -O "Nodes" -n "${used_nodes}"`
#echo "${used_nodes}"

echo "cleanup will execute on nodelist: " ${used_nodes} " corresponding to: " $t_nodes " nodes."

sbatch -J artis_c --nodes=$t_nodes --tasks-per-node=1  --partition=main --time=08:00:00 --mail-type=ALL --mail-user=${USER}@gsi.de --nodelist=$used_nodes --output='%j.log' -- ./rm_tmp.sh


