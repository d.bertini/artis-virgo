#!/bin/bash

myhost=`hostname`
# check if /tmp/$USER exist on the node
MYTMP="/tmp/$USER"

echo "look for local directory: "  $MYTMP on Node: $myhost

if [ -d "$MYTMP" ]; then
    echo "found a local directory: "  $MYTMP on Node: $myhost " removing ..."
    rm -rf $MYTMP
fi
